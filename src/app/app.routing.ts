import { ModuleWithProviders } from '@angular/core';
import { MainComponent } from './main/main.component';
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./authen/login/login.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { RouterModule, Routes } from "@angular/router";
import { AboutMeComponent } from "./about-me/about-me.component";
import { AgreementComponent } from "./agreement/agreement.component";
import { ServiceComponent } from "./service/service.component";

const AppRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '',
    component: MainComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'about-me', component: AboutMeComponent },
      { path: 'agreement', component: AgreementComponent },
      { path: 'service', component: ServiceComponent },
    ],
  },
  { path: 'login', component: LoginComponent },
  { path: '**', component: NotFoundComponent },
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
