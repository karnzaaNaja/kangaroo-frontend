import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
  // public Server = 'http://john888.sbayit.com/';
  public Server = 'http://localhost:8000/';

  public ApiUrl = 'api/';
  public ServerWithUrl = this.Server;
  public ServerWithApiUrl = this.Server + this.ApiUrl;
}
